insert into sexe values(DEFAULT,'HOMME');
insert into sexe values(DEFAULT,'FEMME');

insert into acteur values(DEFAULT,'Michael',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Jhon',2,'author-1.jpg');
insert into acteur values(DEFAULT,'Marie',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Thommy',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Jean',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Jeanne',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Lauv',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Jo',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Ginny',1,'author-2.jpg');
insert into acteur values(DEFAULT,'Marcus',1,'author-2.jpg');


insert into emotion values(DEFAULT,'JOIE');
insert into emotion values(DEFAULT,'TRISTESSE');
insert into emotion values(DEFAULT,'PEUR');
insert into emotion values(DEFAULT,'COLERE');
insert into emotion values(DEFAULT,'SURPRISE');
insert into emotion values(DEFAULT,'DEGOUT');
insert into emotion values(DEFAULT,'EMERVEILLEMENT');
insert into emotion values(DEFAULT,'TENSION');
insert into emotion values(DEFAULT,'ANXIETE');
insert into emotion values(DEFAULT,'DECEPTION');
insert into emotion values(DEFAULT,'FRUSTRATION');
insert into emotion values(DEFAULT,'NOSTALGIE');
insert into emotion values(DEFAULT,'COMPASSION');
insert into emotion values(DEFAULT,'EMPATHIE');
insert into emotion values(DEFAULT,'ENTHOUSIASME');
insert into emotion values(DEFAULT,'AMOUR');
insert into emotion values(DEFAULT,'HAINE');
insert into emotion values(DEFAULT,'DESESPOIR');
insert into emotion values(DEFAULT,'SOULAGEMENT');

insert into geste values (DEFAULT,'marcher');
insert into geste values (DEFAULT,'courir');
insert into geste values (DEFAULT,'se tenir droit');
insert into geste values (DEFAULT,'se pencher');
insert into geste values (DEFAULT,'se lever');
insert into geste values (DEFAULT,'se coucher');
insert into geste values (DEFAULT,'se mettre à genoux');
insert into geste values (DEFAULT,'se prosterner');
insert into geste values (DEFAULT,'sourire');
insert into geste values (DEFAULT,'froncer le sourcils');
insert into geste values (DEFAULT,'cligner des yeux');
insert into geste values (DEFAULT,'lever un sourcil');
insert into geste values (DEFAULT,'plisser les yeux');
insert into geste values (DEFAULT,'hausser les épaules');
insert into geste values (DEFAULT,'tirer la langue');
insert into geste values (DEFAULT,'faire un signe de la main');
insert into geste values (DEFAULT,'pointer du doigt');
insert into geste values (DEFAULT,'faire un geste de la tête');
insert into geste values (DEFAULT,'faire un signe de la croix');
insert into geste values (DEFAULT,'faire un geste de salut');
insert into geste values (DEFAULT,'tenir une arme');
insert into geste values (DEFAULT,'prendre une tasse');
insert into geste values (DEFAULT,'ouvrir une porte');
insert into geste values (DEFAULT,'utiliser un téléphone');
insert into geste values (DEFAULT,'écrire sur une feuille de papier');
insert into geste values (DEFAULT,'pleurer');
insert into geste values (DEFAULT,'rire');
insert into geste values (DEFAULT,'frissoner');
insert into geste values (DEFAULT,'trembler');
insert into geste values (DEFAULT,'crier');
insert into geste values (DEFAULT,'bâiller');
insert into geste values (DEFAULT,'faire un poing');
insert into geste values (DEFAULT,'fumer une cigarette');
insert into geste values (DEFAULT,'ajuster une cravate');
insert into geste values (DEFAULT,'frapper');
insert into geste values (DEFAULT,'donner un coup de pied');
insert into geste values (DEFAULT,'lancer un coup de poing');
insert into geste values (DEFAULT,'parer un coup');

insert into projet values (DEFAULT,'Spiderman','spiderman.jpeg','Tom Holland, alias Peter parker est un super-heros evoluant dans un univers Marvel de la maison edition Marvel Comics. Cree par le scenariste Stan Lee et le dessinateur Jack Kirby, le personnage de fiction apparait pour la premiere fois dans le comic book The Incredible Hulk #1 en mai 1962.');
insert into projet values (DEFAULT,'Mr Bean','mrbean.jpeg','Le Rowan Atkinson, alias Mr bean est un humoriste evoluant dans un univers Marvel de la maison edition Marvel Comics. Cree par le scenariste Stan Lee et le dessinateur Jack Kirby, le personnage de fiction apparait pour la premiere fois dans le comic book The Incredible Hulk #1 en mai 1962.');

insert into categorieplateau values (DEFAULT,'Ville');
insert into categorieplateau values (DEFAULT,'Nature');

insert into plateau values (DEFAULT,'Tour Eifel, Paris, France','Plateau montagneux situe dans les Rocheuses canadiennes','kananaskis.jpg',1,-115.1567,50.9167,2);
insert into plateau values (DEFAULT,'Parc des prices, Paris, France','Le Plateau de Glen Canyon est represente comme une zone desertique et aride de l Arizona','glen.jpg',1,-111.5272,37.1356,2);
insert into plateau values (DEFAULT,'La cite du cinema, Arizona, France','Le plateau est celebre pour ses formations rocheuses rouges et brunes','monument.jpg',1,-110.0764,36.9986,2);
insert into plateau values (DEFAULT,'Les studios des paris, Marseille, France','Ville tres urbaine et très peuplee','sanfransisco.jpg',1,-111.5272,37.1356,1);
insert into plateau values (DEFAULT,'Les studios de la victorine, Cote dAzur, France','Présenté comme un complexe scientifique secret, entoure d un haut mur de sécurité','gamma.jpg',1,-111.5272,37.1356,2);
insert into plateau values (DEFAULT,'Studio de la montjoie, Saint Denis, France','Fort Bragg est montrée comme la ville ou se trouve la base militaire où le pere de Bruce Banner','fort.jpg',1,-123.8053,39.4457,1);
insert into plateau values (DEFAULT,'Universite de paris, France','Montrée comme le lieu de travail de deux personnages importants','universite.jpg',1,-122.2578, 37.8719,1);
insert into plateau values (DEFAULT,'Stade de france, Paris, France','Le Palouse Falls est une cascade qui se trouve dans une gorge profonde creusee par la riviere Palouse','palouse.jpg',1,-118.2313,46.7381,2);

insert into scene values (DEFAULT,'Scene de combat',5,1,0);
insert into scene values (DEFAULT,'Scene de discours',120,1,0);
insert into scene values (DEFAULT,'Scene de negociation',55,1,0);

insert into scene values (DEFAULT,'Scene damour',120,2,0);
insert into scene values (DEFAULT,'Scene de suspense',60,2,0);
insert into scene values (DEFAULT,'Scene daction',40,2,0);
insert into scene values (DEFAULT,'Scene de mort',50,2,0);
insert into scene values (DEFAULT,'Scene de revelation',70,2,0);
insert into scene values (DEFAULT,'Scene de comedie',120,2,0);
insert into scene values (DEFAULT,'Scene de trahison',60,2,0);
insert into scene values (DEFAULT,'Scene de poursuite en voiture',30,2,0);
insert into scene values (DEFAULT,'Scene de poursuite a pieds',25,2,0);
insert into scene values (DEFAULT,'Scene de combat karate',60,2,0);

insert into scene values (DEFAULT,'Scene de course',60,3,0);
insert into scene values (DEFAULT,'Scene de pause cafe',40,3,0);
insert into scene values (DEFAULT,'Scene dhorreur',20,3,0);
insert into scene values (DEFAULT,'Scene dramatique',50,3,0);
insert into scene values (DEFAULT,'Scene romantique',70,3,0);
insert into scene values (DEFAULT,'Scene de dispute',120,3,0);
insert into scene values (DEFAULT,'Scene de discours',30,3,0);
insert into scene values (DEFAULT,'Scene de combat avec armes',10,3,0);
insert into scene values (DEFAULT,'Scene comique',25,3,0);
insert into scene values (DEFAULT,'Scene triste',60,3,0);

insert into scene values (DEFAULT,'Scene de decouverte',80,4,0);
insert into scene values (DEFAULT,'Scene de sacrifice',20,4,0);
insert into scene values (DEFAULT,'Scene de pleurer',120,4,0);
insert into scene values (DEFAULT,'Scene de visite',20,4,0);
insert into scene values (DEFAULT,'Scene de dialogue',80,4,0);
insert into scene values (DEFAULT,'Scene de bataille epique',30,4,0);
insert into scene values (DEFAULT,'Scene emotionnelles',90,4,0);
insert into scene values (DEFAULT,'Scene de violence',20,4,0);
insert into scene values (DEFAULT,'Scene choquante',25,4,0);
insert into scene values (DEFAULT,'Scene de survie',10,4,0);

insert into scene values (DEFAULT,'Scene de combat brutales',80,5,0);
insert into scene values (DEFAULT,'Scene de dialogue rapide',20,5,0);
insert into scene values (DEFAULT,'Scene dramatiques',120,5,0);
insert into scene values (DEFAULT,'Scene disputes',20,5,0);
insert into scene values (DEFAULT,'Scene dexploration',80,5,0);
insert into scene values (DEFAULT,'Scene voyage',30,5,0);
insert into scene values (DEFAULT,'Scene musique',90,5,0);
insert into scene values (DEFAULT,'Scene chanter',20,5,0);
insert into scene values (DEFAULT,'Scene voyage spatial',25,5,0);
insert into scene values (DEFAULT,'Scene sauvegarde',10,5,0);


insert into action values (DEFAULT,'Action 1','Scénario action 1',1);
insert into action values (DEFAULT,'Action 2','Scénario action 2',2);
insert into action values (DEFAULT,'Action 3','Scénario action 3',3);
insert into action values (DEFAULT,'Action 4',' Scénario action 4',4);
insert into action values (DEFAULT,'Action 5','Scénario action 5',5);
insert into action values (DEFAULT,'Action 6','Scénario action 6',6);
insert into action values (DEFAULT,'Action 7','Scénario action 7',7);
insert into action values (DEFAULT,'Action 8','Scénario action 8',8);
insert into action values (DEFAULT,'Action 9','Scénario action 9',9);
insert into action values (DEFAULT,'Action 10','Scénario action 10',10);
insert into action values (DEFAULT,'Action 11','Scénario action 11',11);
insert into action values (DEFAULT,'Action 12','Scénario action 12',12);
insert into action values (DEFAULT,'Action 13','Scénario action 13',13);
insert into action values (DEFAULT,'Action 14','Scénario action 14',14);
insert into action values (DEFAULT,'Action 15','Scénario action 15',15);
insert into action values (DEFAULT,'Action 16','Scénario action 16',16);
insert into action values (DEFAULT,'Action 17','Scénario action 17',17);
insert into action values (DEFAULT,'Action 18','Scénario action 18',18);
insert into action values (DEFAULT,'Action 19','Scénario action 19',19);
insert into action values (DEFAULT,'Action 20','Scénario action 20',20);
insert into action values (DEFAULT,'Action 21','Scénario action 21',21);
insert into action values (DEFAULT,'Action 22','Scénario action 22',22);
insert into action values (DEFAULT,'Action 23','Scénario action 23',23);
insert into action values (DEFAULT,'Action 24','Scénario action 24',24);
insert into action values (DEFAULT,'Action 25','Scénario action 25',25);
insert into action values (DEFAULT,'Action 26','Scénario action 26',26);
insert into action values (DEFAULT,'Action 27','Scénario action 27',27);
insert into action values (DEFAULT,'Action 28','Scénario action 28',28);
insert into action values (DEFAULT,'Action 29','Scénario action 29',29);
insert into action values (DEFAULT,'Action 30','Scénario action 30',30);
insert into action values (DEFAULT,'Action 31','Scénario action 31',31);
insert into action values (DEFAULT,'Action 32','Scénario action 32',32);
insert into action values (DEFAULT,'Action 33','Scénario action 33',33);
insert into action values (DEFAULT,'Action 34','Scénario action 34',34);
insert into action values (DEFAULT,'Action 35','Scénario action 35',35);
insert into action values (DEFAULT,'Action 36','Scénario action 36',36);
insert into action values (DEFAULT,'Action 37','Scénario action 37',37);
insert into action values (DEFAULT,'Action 38','Scénario action 38',38);
insert into action values (DEFAULT,'Action 39','Scénario action 39',39);
insert into action values (DEFAULT,'Action 40','Scénario action 40',40);

insert into action values (DEFAULT,'Action 41','Scénario action 41',41);
insert into action values (DEFAULT,'Action 42','Scénario action 42',42);
insert into action values (DEFAULT,'Action 43','Scénario action 43',43);
insert into action values (DEFAULT,'Action 44','Scénario action 44',44);


insert into detailsAction values (DEFAULT,1,1,'Détails action 1',1,1);
insert into detailsAction values (DEFAULT,2,2,'Détails action 2',2,2);
insert into detailsAction values (DEFAULT,3,3,'Détails action 3',3,3);
insert into detailsAction values (DEFAULT,4,3,'Détails action 4',4,4);
insert into detailsAction values (DEFAULT,5,5,'Détails action 5',5,5);
insert into detailsAction values (DEFAULT,7,7,'Détails action 6',6,6);
insert into detailsAction values (DEFAULT,6,4,'Détails action 7',7,7);
insert into detailsAction values (DEFAULT,8,4,'Détails action 8',8,8);
insert into detailsAction values (DEFAULT,9,5,'Détails action 9',9,9);
insert into detailsAction values (DEFAULT,10,6,'Détails action 10',10,10);
insert into detailsAction values (DEFAULT,11,8,'Détails action 11',11,11);
insert into detailsAction values (DEFAULT,12,9,'Détails action 12',12,12);
insert into detailsAction values (DEFAULT,13,2,'Détails action 13',13,13);
insert into detailsAction values (DEFAULT,14,2,'Détails action 14',14,14);
insert into detailsAction values (DEFAULT,15,3,'Détails action 15',15,15);
insert into detailsAction values (DEFAULT,16,9,'Détails action 16',16,16);
insert into detailsAction values (DEFAULT,17,7,'Détails action 17',18,17);
insert into detailsAction values (DEFAULT,18,9,'Détails action 18',19,18);
insert into detailsAction values (DEFAULT,19,6,'Détails action 19',1,19);
insert into detailsAction values (DEFAULT,20,5,'Détails action 20',2,20);
insert into detailsAction values (DEFAULT,21,9,'Détails action 21',3,21);
insert into detailsAction values (DEFAULT,22,1,'Détails action 22',4,22);
insert into detailsAction values (DEFAULT,23,1,'Détails action 23',5,23);
insert into detailsAction values (DEFAULT,24,2,'Détails action 24',6,24);
insert into detailsAction values (DEFAULT,25,3,'Détails action 25',7,25);
insert into detailsAction values (DEFAULT,26,4,'Détails action 26',8,26);
insert into detailsAction values (DEFAULT,27,6,'Détails action 27',9,27);
insert into detailsAction values (DEFAULT,28,5,'Détails action 28',10,28);
insert into detailsAction values (DEFAULT,29,5,'Détails action 29',11,29);
insert into detailsAction values (DEFAULT,30,5,'Détails action 30',12,30);
insert into detailsAction values (DEFAULT,31,8,'Détails action 31',13,31);
insert into detailsAction values (DEFAULT,32,9,'Détails action 32',14,32);
insert into detailsAction values (DEFAULT,33,3,'Détails action 33',15,33);
insert into detailsAction values (DEFAULT,34,7,'Détails action 34',16,34);
insert into detailsAction values (DEFAULT,35,7,'Détails action 35',17,35);
insert into detailsAction values (DEFAULT,36,8,'Détails action 36',18,36);
insert into detailsAction values (DEFAULT,37,5,'Détails action 37',19,37);
insert into detailsAction values (DEFAULT,38,4,'Détails action 38',1,38);
insert into detailsAction values (DEFAULT,39,3,'Détails action 39',2,1);
insert into detailsAction values (DEFAULT,40,2,'Détails action 40',3,2);
insert into detailsAction values (DEFAULT,41,7,'Détails action 41',4,3);
insert into detailsAction values (DEFAULT,42,4,'Détails action 42',5,4);
insert into detailsAction values (DEFAULT,43,1,'Détails action 43',6,5);


create or replace view DetailsActionView as 
select da.rowid,da.idAction,a.nom as acteur,a.nomImage,da.phrase,e.nom as emotion,g.nom as geste from detailsAction as da join acteur as a on da.idActeur=a.idActeur 
join emotion as e on da.idEmotion=e.idEmotion 
join geste as g on da.idGeste=g.idGeste;

create or replace view SceneActeurView as 
select da.rowid,s.idscene,da.idacteur from scene as s join action as a on s.idscene=a.idscene join detailsaction as da on a.idaction=da.idaction;

create or replace view newview as 
select acteur.nom as nomActeur, scene.nom as nomScene, action.nom as nomAction,detailsAction.phrase from detailsaction
join acteur on detailsaction.idacteur = acteur.idActeur
join action on detailsaction.idaction = action.idaction
join scene on action.idscene = scene.idscene;
