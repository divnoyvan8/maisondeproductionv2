<%-- 
    Document   : ajoutaction
    Created on : 1 mars 2023, 08:40:54
    Author     : P14A_77_Michael
--%>


<%@page import="java.util.List"%>
<%@page import="model.Geste"%>
<%@page import="model.Acteur"%>
<%@page import="model.Emotion"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.util.ArrayList"%>
<%int idAction = (Integer) request.getAttribute("idAction");
int idDetail_action = (Integer) request.getAttribute("idDetails_action");
String scenario = (String) request.getAttribute("scenario");
    List<Emotion> le = (List<Emotion>) request.getAttribute("le");
    List<Acteur> lact = (List<Acteur>) request.getAttribute("lact");
    List<Geste> lg = (List<Geste>) request.getAttribute("lg");
    
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>


    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />





    </head>

    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">
                                            <li class="active"> <a style="font-size:19px;"  href="<%=request.getContextPath()%>/">Accueil</a> </li>
                                            <li><a style="font-size:19px;" href="">Planning</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="assets/icon/email.png" />nymalaza@gmail.com</li>
                                    <li style="font-size:17px;"><img src="assets/icon/loc.png" />Location</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <br>
        <br>
        <br>

    <body>


        <a href="acceuil.jsp"></a>
        <div class="container-fluid">
            <center><h3 class="text-dark mb-2">Ajouter scène</h3></center>

            <div class="col-lg-8"></div>
            <div class="col-lg-12">
                <form action="<%=request.getContextPath()%>/updateDetailsActions">
                    <div class="card-body">                                                     
                        <div class="row">
                            <div class="col">                                                                                     
                                <div class="mb-3"><input value="<%out.println(idDetail_action);%>" name="idDetails_action" hidden></div> 
                                <div class="mb-3"><input value="<%out.println(idAction);%>" name="idAction" hidden></div>                     
                                <label class="form-label"><strong>Scenario: </strong><br></label><input class="form-control" id="signature" value="<%out.println(scenario);%>" rows="4" name="scenario"></p>
                                <div class="mb-3"><label class="form-label" for="type" style="color: #0b2e130;"><strong>Emotions : </strong></label></br>
                                    <select class="form-control-user" name="emotion">
                                        <%for (int i = 0; i < le.size(); i++) {
                                        %>
                                        <option value="<%out.print(le.get(i).getIdEmotion());%>"><%out.print(le.get(i).getNom());%></option>
                                        <%}%>
                                    </select>  </div> 
                                <label class="form-label" for="type" style="color: #0b2e130;"><strong>Geste : </strong></label></br>
                                <select class="form-control-user" name="geste">
                                    <%for (int i = 0; i < lg.size(); i++) {
                                    %>
                                    <option value="<%out.print(lg.get(i).getIdGeste());%>"><%out.print(lg.get(i).getNom());%></option>
                                    <%}%>
                                </select>                              
                                <div class="mb-3"><label class="form-label" for="type" style="color: #0b2e130;"><strong>Acteur : </strong></label></br>
                                    <select class="form-control-user" name="acteur">
                                        <%for (int i = 0; i < lact.size(); i++) {
                                        %>
                                        <option value="<%out.print(lact.get(i).getIdActeur());%>"><%out.print(lact.get(i).getNom());%></option>
                                        <%}%>
                                    </select>  </div> 
                                <a style="font-size:19px;"<center> <button  style="border-color: black;background-color: black"class="btn btn-primary d-block btn-user w-10" >AJOUTER ACTION
                                        </button></center></a>
                            </div>
                        </div> 
                    </div>           
                </form>
            </div>
            <div class="col-lg-4"></div>

        </div>
    </body>

    <footer>
        <div id="contact" class="footer">
            <div class="container">
                <div class="row pdn-top-30">
                    <div class="col-md-12 ">
                        <div class="footer-box">
                            <div class="headinga">
                                <h3>Address</h3>
                                <span>Paris | France </span>
                                <p>(+261) 8522369417
                                    <br>jolympique@gmail.com</p>
                            </div>
                            <ul class="location_icon">
                                <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                            </ul>
                            <div class="menu-bottom">
                                <ul class="link">
                                    <li> <a href="">Actualiser</a></li>
                                    <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <marquee> <p>© RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="js/plugin.js"></script>

    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/custom.js"></script>

    <script src="js/owl.carousel.js"></script>
</body>

</html>
