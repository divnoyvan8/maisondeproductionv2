<%-- 
    Document   : listeClient
    Created on : 3 f�vr. 2023, 16:46:49
    Author     : P14A_77_Michael
--%>

<%@page import="model.Scene"%>
<%@page import="java.util.List"%>
<%@page import="model.Plateau"%>
<%@page import="model.Projet"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%
    Projet p = (Projet) request.getAttribute("p");
    List<Plateau> lpl = (List<Plateau>) request.getAttribute("lpl");
    List<Scene> lsc = (List<Scene>) request.getAttribute("lsc");
%>

<html>

    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/css1/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css"href="http://localhost:8080/Actu_-_Copie/assets/csss/Search.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Basic.css'); ?>">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Clean.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/ionicons.min.css">

    </head>
    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">
                                        <img src="http://localhost:8080/Actu_-_Copie/assets/images/logo1.png" alt="icon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">

                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/">Acceuil</a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/planning">Planning</a> </li>
                                            <li class="search">
                                                <form action="<%=request.getContextPath()%>/recherche">
                                                    <input style="background-color: black;border-radius: 20px;color:white; border: 0.5px" type="text" name="recherche">
                                                    <button style="background: black;border: 0px" type="submit" ><img type="submit" src="http://localhost:8080/Actu_-_Copie/assets/images/search_icon.png" alt="icon" /></button>
                                                </form>
                                            </li>

                                        </ul>

                                    </nav>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/email.png" />mymovies@gmail.com</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/loc.png" />mymovies</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>


    <body>

        <section class="bg-light" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;"> Choix des sc�nes</h4><br>
                        <p class="text-muted">mymovies</p>
                         <a href="${pageContext.request.contextPath}/planning_date?idProjet=<%=p.getIdProjet()%>"><button class="btn btn-primary btn-sm" style="background-color:green;float: right;border-color: green">Plannification</button></a>
                    </div>
                </div>

                <div class="row">

                    <div class="col-sm-12 col-md-12 portfolio-item">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"></i></div>


                            <div class="portfolio-caption">

                                <div class="auto-container">

                                    <% for (Plateau pl : lpl) {%>
                                    <div>
                                        <h3 style="color:purple"><strong><%=pl.getNom()%></strong></h3>
                                        <br/>
                                        <div class="row clearfix">

                                            <% for (Scene sc : lsc) {%>
                                            <% if (sc.getIdPlateau() == pl.getIdPlateau()) {%>
                                            <!-- Service Block -->
                                            <div class="service-block-two col-lg-4 col-md-6 col-sm-12">
                                                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                                    <div class="content">
                                                        <div class="icon-box">
                                                            <span class="icon flaticon-sketch"></span>
                                                        </div>
                                                        <h6><%=sc.getNom()%></h6>
                                                        <div class="text"><strong>Dur�e : </strong> <%=sc.getDuree()%>min.</div>
                                                        <a href="${pageContext.request.contextPath}/ajout_planning?idScene=<%=sc.getIdScene()%>&idProjet=<%=p.getIdProjet()%>" ><button class="btn btn-primary btn-sm">Click</button></a>
                                                 
                                                    </div>
                                                </div>
                                            </div>
                                            <%}%>
                                            <%}%>

                                        </div>
                                    </div>
                                    <%}%>

                                   

                                </div> 
                            </div>
                        </div>



                    </div>

                </div>
        </section>
                <!--    <section class="page-title" style="background-image:url(${pageContext.request.contextPath}/resources/images/background/scene.jpg)">
                <div class="auto-container">
                    <h2>Choix Des Sc�nes</h2>
                    <ul class="page-breadcrumb">
                        <li><a href="${pageContext.request.contextPath}/"><%=p.getNom()%></a></li>
                        <li>Planning</li>
                    </ul>
                </div>
            </section>
            End Page Title-->

        <!-- Portfolio Page Section -->
        <section class="portfolio-page-section">

        </section>










        <%
            // String date = sujet.get(i).getDatedepot();
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
            //LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
            /* String date2 = sujet.get(i).getDatepublication(); 
DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                              LocalDateTime localDateTime1 = LocalDateTime.parse(date2, formatter);
                if ( localDateTime1.getHour() == LocalDateTime.now().getHour()
                        && localDateTime1.getMinute() == LocalDateTime.now().getMinute()) { 
                   out.println("mety");  */
        %>   



    </div>
</div>
</div>
</body>

<footer>
    <div id="contact" class="footer">
        <div class="container">
            <div class="row pdn-top-30">
                <div class="col-md-12 ">
                    <div class="footer-box">
                        <div class="headinga">
                            <h3>Address</h3>
                            <span>Paris | France </span>
                            <p>(+261) 8522369417
                                <br>jolympique@gmail.com</p>
                        </div>
                        <ul class="location_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                        </ul>
                        <div class="menu-bottom">
                            <ul class="link">
                                <li> <a href="">Actualiser</a></li>
                                <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <marquee> <p>� RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/plugin.js"></script>

<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/owl.carousel.js"></script>
</body>

</html>
