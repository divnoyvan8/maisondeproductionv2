<%-- 
    Document   : listeClient
    Created on : 3 f�vr. 2023, 16:46:49
    Author     : P14A_77_Michael
--%>

<%@page import="model.Plateau"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="model.DateTournage"%>
<%@page import="model.Projet"%>
<%@page import="java.util.HashMap"%>
<%@page import="model.Scene"%>
<%@page import="java.util.List"%>
<%
    Projet p = (Projet) request.getAttribute("p");
    List<Plateau> lpl = (List<Plateau>) request.getAttribute("lpl");
    DateTournage dt = (DateTournage) request.getAttribute("dt");
    List<Date> ldt = (List<Date>) request.getAttribute("ldt");
    List<HashMap<Date, Scene>> planning = (List<HashMap<Date, Scene>>) request.getAttribute("planning");
    List<HashMap<Date, Double>> lthdt = (List<HashMap<Date, Double>>) request.getAttribute("lthdt");
    List<Scene> lscnp = (List<Scene>) request.getAttribute("lscnp");
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
%>

<html>

    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/css1/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css"href="http://localhost:8080/Actu_-_Copie/assets/csss/Search.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Basic.css'); ?>">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Clean.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/ionicons.min.css">

    </head>
    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">
                                        <img src="http://localhost:8080/Actu_-_Copie/assets/images/logo1.png" alt="icon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">

                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/">Acceuil</a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/planning">Planning</a> </li>
                                            <li class="search">
                                                <form action="<%=request.getContextPath()%>/recherche">
                                                    <input style="background-color: black;border-radius: 20px;color:white; border: 0.5px" type="text" name="recherche">
                                                    <button style="background: black;border: 0px" type="submit" ><img type="submit" src="http://localhost:8080/Actu_-_Copie/assets/images/search_icon.png" alt="icon" /></button>
                                                </form>
                                            </li>

                                        </ul>

                                    </nav>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/email.png" />mymovies@gmail.com</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/loc.png" />mymovies</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>


    <body>



        <section class="bg-light" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;"> Nos projets</h4><br>
                        <h6>Planning :  <%=dt.getDateDebut()%> - <%=dt.getDateFin()%> </h6>
                        <p class="text-muted">mymovies</p>
                    </div>
                </div>
                
                <div class="row">
                    
                     <div class="auto-container">
                <h4 style="color: black;"><strong>Les sc�nes planifi�es</strong></h4>
                <hr/>
                <% for (Date d : ldt) {%>
                <div>
                    <h5 style="color: goldenrod;"><strong>Sc�ne le <%=sdf.format(d)%></strong></h5>
                    <br>

                    <div>
                        <% for (HashMap<Date, Double> thdt : lthdt) {%>
                        <% if (thdt.get(d) != null) {%>
                        <h4>Dur�e totale : <%=thdt.get(d)%> min</h4>
                        <br/>
                        <%}%>
                        <%}%>
                        <div class="row clearfix">
                            <% for (HashMap<Date, Scene> plan : planning) {%>
                            <% if (plan.get(d) != null) {%>
                            <% Scene sc = plan.get(d);%>
                            <!-- Service Block -->
                            <div class="service-block-two col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="content">
                                        <div class="icon-box">
                                            <span class="icon flaticon-sketch"></span>
                                        </div>
                                        <h3 style="color:purple"><%=sc.getNom()%></a></h3>
                                        <div class="text"><strong>Dur�e : </strong> <%=sc.getDuree()%>min.</div>
                                        <% for (Plateau pl : lpl) {%>
                                        <% if (sc.getIdPlateau() == pl.getIdPlateau()) {%>
                                        <div class="text"><strong>Plateau :</strong> <%=pl.getNom()%></div>
                                        <%}%>
                                        <%}%>
                                        <a href="${pageContext.request.contextPath}/liste_action?idScene=<%=sc.getIdScene()%>"><button class="btn btn-primary btn-sm" style="background-color:black;float: center;border-color: black">details</button></a>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                            <%}%>
                        </div>
                    </div>

                </div>
                        <br>
                <hr/>
                <%}%>

                <div>

                    <h2 style="color: black;"><strong>Sc�nes non planifi�es</strong></h2>
                    <br><br>
                    <div>
                        <div class="row clearfix">
                            <% for (Scene sc : lscnp) {%>
                            <!-- Service Block -->
                            <div class="service-block-two col-lg-4 col-md-6 col-sm-12">
                                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                    <div class="content">
                                        <div class="icon-box">
                                            <span class="icon flaticon-sketch"></span>
                                        </div>
                                        <h3 style="color:purple"><%=sc.getNom()%></a></h3>
                                        <div class="text"><strong>Dur�e : </strong> <%=sc.getDuree()%>min.</div>
                                        <button class="btn btn-primary btn-sm" style="background-color:black;float: center;border-color: black">details</button></a>
                                    </div>
                                </div>
                            </div>
                            <%}%>
                        </div>
                    </div>

                </div>

            </div>
                    
                </div>





            </div>
        </section>


    

    </div>
</div>
</div>
</body>

<footer>
    <div id="contact" class="footer">
        <div class="container">
            <div class="row pdn-top-30">
                <div class="col-md-12 ">
                    <div class="footer-box">
                        <div class="headinga">
                            <h3>Address</h3>
                            <span>Paris | France </span>
                            <p>(+261) 8522369417
                                <br>jolympique@gmail.com</p>
                        </div>
                        <ul class="location_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                        </ul>
                        <div class="menu-bottom">
                            <ul class="link">
                                <li> <a href="">Actualiser</a></li>
                                <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <marquee> <p>� RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/plugin.js"></script>

<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/owl.carousel.js"></script>
</body>

</html>
