/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dao.HibernateDao;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import static javax.ws.rs.HttpMethod.GET;
import javax.ws.rs.Path;
import model.Acteur;
import model.ActeurNonDispo;
import model.CategoriePlateau;
import model.Plateau;
import model.Projet;
import model.Scene;
import model.Action;
import model.DateTournage;
import model.Details_action;
import model.Emotion;
import model.Geste;
import model.Planning;
import model.PlateauNonDispo;
import model.SceneActeurView;
import model.V_planning;
import model.V_scene_duree;
import static org.hibernate.CacheMode.GET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import utilitaires.Api;

/**
 *
 * @author
 */
@Controller
public class SpringController {

    @Autowired
    HibernateDao dao;

    //REDIRECTION
    @RequestMapping(value = "/redirectionAjoutScene", method = RequestMethod.GET)
    public String redirectAjoutScene(Model model, @RequestParam(required = true) Integer idPlateau) {
        model.addAttribute("idPlateau", idPlateau);
        return "ajoutScene";
    }

    @RequestMapping(value = "/redirectionAjoutAction", method = RequestMethod.GET)
    public String redirectAjoutAction(Model model, @RequestParam(required = true) Integer idScene) {
        model.addAttribute("idScene", idScene);
        return "ajoutAction";
    }

    @RequestMapping(value = "/redirectionUpdate", method = RequestMethod.GET)
    public String redirectionUpdate(Model model, HttpServletRequest request) {
        int idDetails_action = Integer.parseInt(request.getParameter("idDetails_action"));
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        String scenario = request.getParameter("scenario");
        model.addAttribute("idDetails_action", idDetails_action);
        model.addAttribute("scenario", scenario);
        model.addAttribute("idAction", idAction);
        List<Emotion> emotion = dao.findAll(Emotion.class);
        model.addAttribute("le", emotion);
        List<Geste> geste = dao.findAll(Geste.class);
        model.addAttribute("lg", geste);
        List<Acteur> acteur = dao.findAll(Acteur.class);
        model.addAttribute("lact", acteur);
        return "updateaction";
    }

    //------------------------------------------------------------------------------------------------------------------------------------------
    //CRUD SELECT
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        List<Projet> lp = dao.findAll(Projet.class);
        model.addAttribute("lp", lp);
        return "acceuil";
    }

    @RequestMapping(value = "/planning", method = RequestMethod.GET)
    public String planning(Model model) {
        List<Projet> lp = dao.findAll(Projet.class);
        model.addAttribute("lp", lp);
        return "planning";
    }

    @RequestMapping(value = "/listeplateau", method = RequestMethod.GET)
    public String liste_plateau(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        List<CategoriePlateau> lcp = dao.findAll(CategoriePlateau.class);
        model.addAttribute("lcp", lcp);
        Plateau plateau = new Plateau();
        plateau.setIdProjet(idProjet);
        List<Plateau> lp = dao.findWhere(plateau);
        model.addAttribute("lp", lp);
        return "listeplateau";
    }

    @RequestMapping(value = "/listescene", method = RequestMethod.GET)
    public String liste_scene(Model model, @RequestParam(required = true) Integer idPlateau) {
        Plateau plateau = dao.findById(Plateau.class, idPlateau);
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdPlateau());
        model.addAttribute("p", projet);
        V_scene_duree v = new V_scene_duree();
        v.setIdplateau(idPlateau);
        List<V_scene_duree> lsc = dao.findWhere(v);
        model.addAttribute("lsc", lsc);
        return "listescene";
    }

    /*@RequestMapping(value = "/listescene", method = RequestMethod.GET)
    public String liste_scene(Model model, @RequestParam(required = true) Integer idPlateau) {
        Plateau plateau = dao.findById(Plateau.class, idPlateau);
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdPlateau());
        model.addAttribute("p", projet);
        Scene s = new Scene();
        s.setIdPlateau(idPlateau);
        List<Scene> lsc = dao.findWhere(s);
        model.addAttribute("lsc", lsc);
        return "listescene";
    }*/
    @RequestMapping(value = "/getPlanning", method = RequestMethod.GET)
    public String getPlanning(Model model) {
        List<Planning> lp = dao.findAll(Planning.class);
        model.addAttribute("lp", lp);
        return "getplanning";
    }

    @RequestMapping(value = "/listeaction", method = RequestMethod.GET)
    public String liste_action(Model model, @RequestParam(required = true) Integer idScene) {
        Scene scene = dao.findById(Scene.class, idScene);
        model.addAttribute("lsc", scene);
        Plateau plateau = dao.findById(Plateau.class, scene.getIdPlateau());
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdProjet());
        model.addAttribute("p", projet);
        Action a = new Action();
        a.setIdScene(idScene);
        List<Action> la = dao.findWhere(a);
        model.addAttribute("la", la);
        return "listeaction";
    }

    @RequestMapping(value = "/detailsaction", method = RequestMethod.GET)
    public String details_action(Model model, @RequestParam(required = true) Integer idAction) {
        Action action = dao.findById(Action.class, idAction);
        model.addAttribute("lac", action);
        Scene scene = dao.findById(Scene.class, action.getIdScene());
        model.addAttribute("lsc", scene);
        Plateau plateau = dao.findById(Plateau.class, scene.getIdPlateau());
        model.addAttribute("pl", plateau);
        Projet projet = dao.findById(Projet.class, plateau.getIdProjet());
        model.addAttribute("p", projet);
        Details_action a = new Details_action();
        a.setIdAction(idAction);
        List<Details_action> la = dao.findWhere(a);
        model.addAttribute("la", la);
        List<Emotion> emotion = dao.findAll(Emotion.class);
        model.addAttribute("le", emotion);
        List<Geste> geste = dao.findAll(Geste.class);
        model.addAttribute("lg", geste);
        List<Acteur> acteur = dao.findAll(Acteur.class);
        model.addAttribute("lact", acteur);
        return "listedetailsaction";
    }

    //---------------------------------------------------------------------------------------------------------------------------------------------
    //CRUD INSERT
    @RequestMapping(value = "/ajoutDetailsAction")
    public String insertDetailsAction(Model model, HttpServletRequest request) {
        int idActeur = Integer.parseInt(request.getParameter("acteur"));
        int idGeste = Integer.parseInt(request.getParameter("geste"));
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        int idEmotions = Integer.parseInt(request.getParameter("emotion"));
        String scenario = request.getParameter("scenario");
        Details_action action = new Details_action();
        action.setIdAction(idAction);
        action.setPhrase(scenario);
        action.setIdEmotion(idEmotions);
        action.setIdGeste(idGeste);
        action.setIdActeur(idActeur);
        dao.create(action);
        model.addAttribute("id", idAction);
        return "redirect1";
    }

    @RequestMapping(value = "/ajoutScene")
    public String insertScene(Model model, HttpServletRequest request) {
        int idPlateau = Integer.parseInt(request.getParameter("idPlateau"));
        String scenario = request.getParameter("scenario");
        Scene scene = new Scene();
        scene.setIdPlateau(idPlateau);
        //scene.setDuree(duree);
        scene.setNom(scenario);
        dao.create(scene);
        model.addAttribute("id", idPlateau);
        return "redirect";
    }

    /*@RequestMapping(value = "/ajoutAction")
    public String insertAction(Model model, HttpServletRequest request) {
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        String scenario = request.getParameter("scenario");
        int duree = Integer.parseInt(request.getParameter("duree"));
        Action action = new Action();
        action.setIdScene(idScene);
        action.setScenario(scenario);
        action.setDuree(duree);
        dao.create(action);
        model.addAttribute("id", idScene);
        return "redirect2";
    }*/
    //------------------------------------------------------------------------------------------------------------------------------------------
    //CRUD UPDATE
    @RequestMapping(value = "/updateDetailsActions")
    public String updateAction(Model model, HttpServletRequest request) {
        int idDetails_action = Integer.parseInt(request.getParameter("idDetails_action"));
        int idActeur = Integer.parseInt(request.getParameter("acteur"));
        int idGeste = Integer.parseInt(request.getParameter("geste"));
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        int idEmotions = Integer.parseInt(request.getParameter("emotion"));
        String scenario = request.getParameter("scenario");
        Details_action action = new Details_action();
        action.setIdAction(idAction);
        action.setIdGeste(idGeste);
        action.setIdActeur(idActeur);
        action.setPhrase(scenario);
        action.setIdEmotion(idEmotions);
        action.setIdDetails_action(idDetails_action);
        dao.update(action);
        model.addAttribute("id", idAction);
        return "redirect1";
    }

    //------------------------------------------------------------------------------------------------------------------------------------------
    //CRUD DELETE
    @RequestMapping(value = "/deleteScene")
    public String deleteScene(Model model, HttpServletRequest request) {
        int idplateau = Integer.parseInt(request.getParameter("idPlateau"));
        int idScene = Integer.parseInt(request.getParameter("idScene"));
        Scene scene = new Scene();
        scene.setIdScene(idScene);
        dao.delete(scene);
        model.addAttribute("id", idplateau);
        return "redirect";
    }

    @RequestMapping(value = "/deleteDetailsAction")
    public String deleteAction(Model model, HttpServletRequest request) {
        int idDetails_action = Integer.parseInt(request.getParameter("idDetails_action"));
        int idAction = Integer.parseInt(request.getParameter("idAction"));
        Details_action da = new Details_action();
        da.setIdDetails_action(idDetails_action);
        dao.delete(da);
        model.addAttribute("id", idAction);
        return "redirect1";
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------
    //FONCTIONALITES
    @RequestMapping(value = "/plannings", method = RequestMethod.GET)
    public String planning(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        V_planning p = new V_planning();
        p.setIdProjet(idProjet);
        List<V_planning> lp = dao.findWhere(p);
        model.addAttribute("lp", lp);
        List<Scene> lsc = dao.findAll(Scene.class, "duree", true);
        model.addAttribute("lsc", lsc);
        return "getplanning";
    }

    @RequestMapping(value = "/recherche", method = RequestMethod.GET)
    public String recherche(Model model, HttpServletRequest request) {
        String recherche = request.getParameter("recherche");
        List<Projet> sujet = new ArrayList<>();
        sujet = dao.findWhereOr(recherche);
        model.addAttribute("liste", sujet);
        return "listerecherche";
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping(value = "/planning_select", method = RequestMethod.GET)
    public String planning_select(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        Plateau pl = new Plateau();
        pl.setIdProjet(idProjet);
        List<Plateau> lpl = dao.findWhere(pl);
        model.addAttribute("lpl", lpl);
        Scene sc = new Scene();
        sc.setEtat(0);
        List<Scene> lsc = dao.findWhere(sc);
        model.addAttribute("lsc", lsc);
        return "planning_select";
    }

    @RequestMapping(value = "/ajout_planning", method = RequestMethod.GET)
    public String ajout_planning(Model model, @RequestParam(required = true) Integer idScene, @RequestParam(required = true) Integer idProjet) {
        Scene sc = dao.findById(Scene.class, idScene);
        sc.setEtat(1);
        dao.update(sc);
        return planning_select(model, idProjet);
    }

    @RequestMapping(value = "/planning_date", method = RequestMethod.GET)
    public String planning_date(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        DateTournage dt = new DateTournage();
        dt.setIdProjet(idProjet);
        List<DateTournage> ldt = dao.findWhere(dt);
        model.addAttribute("ldt", ldt);
        return "planning_date";
    }

    @RequestMapping(value = "/redirectajoutcontrainte", method = RequestMethod.GET)
    public String ajoutContrainte(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        List<Plateau> lpl = dao.findAll(Plateau.class);
        model.addAttribute("lpl", lpl);
        List<Acteur> la = dao.findAll(Acteur.class);
        model.addAttribute("la", la);
        return "ajoutcontrainte";
    }

    @RequestMapping(value = "/redirectajoutdatetournage", method = RequestMethod.GET)
    public String ajoutdatetournage(Model model, @RequestParam(required = true) Integer idProjet) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        return "ajoutdatetournage";
    }

    @RequestMapping(value = "/ajout_dateTournage", method = RequestMethod.GET)
    public String ajout_dateTournage(Model model, @RequestParam(required = true) Integer idProjet, @RequestParam String dateDebut, @RequestParam String dateFin) {
        DateTournage dt = new DateTournage();
        dt.setIdProjet(idProjet);
        try {
            java.util.Date db = new SimpleDateFormat("yyyy-MM-dd").parse(dateDebut);
            java.util.Date df = new SimpleDateFormat("yyyy-MM-dd").parse(dateFin);
            dt.setDateDebut(new SimpleDateFormat("dd/MM/yyyy").format(db));
            dt.setDateFin(new SimpleDateFormat("dd/MM/yyyy").format(df));
            dao.create(dt);
        } catch (ParseException e) {
            model.addAttribute(e);
        }
        return planning_date(model, idProjet);
    }

    @RequestMapping(value = "/plannification", method = RequestMethod.GET)
    public String planning(Model model, @RequestParam(required = true) Integer idProjet, @RequestParam Integer dateid) {
        Projet projet = dao.findById(Projet.class, idProjet);
        model.addAttribute("p", projet);
        Scene sc = new Scene();
        sc.setEtat(1);
        List<Scene> lsc = dao.findWhere(sc);
        List<PlateauNonDispo> lpnd = dao.findAll(PlateauNonDispo.class);
        List<SceneActeurView> lsca = dao.findAll(SceneActeurView.class);
        List<ActeurNonDispo> land = dao.findAll(ActeurNonDispo.class);
        Object[] results;
        DateTournage dt = dao.findById(DateTournage.class, dateid);
        model.addAttribute("dt", dt);
        List<Plateau> lpl = dao.findAll(Plateau.class);
        model.addAttribute("lpl", lpl);
        try {
            results = Api.planning(lsc, lpnd, lsca, land, dt.getDateDebut(), dt.getDateFin());
            List<java.util.Date> ldt = (List<java.util.Date>) results[0];
            List<HashMap<java.util.Date, Scene>> planning = (List<HashMap<java.util.Date, Scene>>) results[1];
            List<HashMap<java.util.Date, Double>> lthdt = (List<HashMap<java.util.Date, Double>>) results[2];
            List<Scene> lscnp = (List<Scene>) results[3];
            model.addAttribute("ldt", ldt);
            model.addAttribute("planning", planning);
            model.addAttribute("lthdt", lthdt);
            model.addAttribute("lscnp", lscnp);
        } catch (Exception e) {
            model.addAttribute("exception", e);
            return "exception";
        }
        return "plannification";
    }

    @RequestMapping(value = "/ajout_contrainte", method = RequestMethod.GET)
    public String ajout_contrainte(Model model, @RequestParam(required = true) Integer idProjet, @RequestParam(required = false) Integer idActeur, @RequestParam java.sql.Date date) {
        ActeurNonDispo and = new ActeurNonDispo();
        and.setIdActeur(idActeur);
        and.setDateNonDispo(date);
        dao.create(and);
        return planning_date(model, idProjet);
    }
}
