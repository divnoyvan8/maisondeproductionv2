
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the tem
plate in the editor.
 */
package utilitaires;

/**
 *
 * @author P14A_77_Michael
 */
public class Utilitaires {

    public static String getNomEmotions(int status) throws Exception {
        String retour = "";
        if (status == 1) {
            return retour = "JOIE";
        }
        if (status == 2) {
            return retour = "TRISTESSE";
        }
        if (status == 3) {
            return retour = "PEUR";
        }
        if (status == 4) {
            return retour = "COLERE";
        }
        if (status == 5) {
            return retour = "SURPRISE";
        }
        if (status == 6) {
            return retour = "DEGOUT";
        }
        if (status == 7) {
            return retour = "EMERVEILLEMENT";
        }
        if (status == 8) {
            return retour = "TENSION";
        }
        if (status == 9) {
            return retour = "ANXIETE";
        }
        if (status == 10) {
            return retour = "DECEPTION";
        }
        if (status == 11) {
            return retour = "FRUSTRATION";
        }
        if (status == 12) {
            return retour = "NOSTALGIE";
        }
        if (status == 13) {
            return retour = "COMPASSION";
        }
        if (status == 14) {
            return retour = "EMPATHIE";
        }
        if (status == 15) {
            return retour = "ENTHOSIASME";
        }
        if (status == 16) {
            return retour = "AMOUR";
        }
        if (status == 17) {
            return retour = "HAINE";
        }
        if (status == 18) {
            return retour = "DESESPOIR";
        }
        if (status == 19) {
            return retour = "SOULAGEMENT";
        }
        return retour;
    }

    public static String getNomActeur(int status) throws Exception {
        String retour = "";
        if (status == 1) {
            return retour = "TOM HOLLAND";
        }
        if (status == 3) {
            return retour = "BENEDICT CUMBERBATCH";
        }
        if (status == 4) {
            return retour = "SCARTELLETE JOHNSON";
        }
        if (status == 5) {
            return retour = "CHADWICK BOSEMAN";
        }
        if (status == 6) {
            return retour = "CHRIS EVAN";
        }
        return retour;
    }

    public static String getNomGeste(int status) throws Exception {
        String retour = "";
        if (status == 1) {
            return retour = "Marcher";
        }
        if (status == 2) {
            return retour = "Courrir";
        }
        if (status == 3) {
            return retour = "Se tenir droit";
        }
        if (status == 4) {
            return retour = "Se pencerr";
        }
        if (status == 5) {
            return retour = "Se leverr";
        }
        if (status == 7) {
            return retour = "Se mettre à genoux";
        }
        if (status == 8) {
            return retour = "Se prostener";
        }
        if (status == 9) {
            return retour = "Sourire";
        }
        if (status == 10) {
            return retour = "Foncer le sourcils";
        }
        if (status == 11) {
            return retour = "Cligner des yeux";
        }
        if (status == 12) {
            return retour = "Lever un sourcil";
        }
        if (status == 13) {
            return retour = "Plisser les yeux";
        }

        if (status == 14) {
            return retour = "Hausser les épaules";
        }
        if (status == 15) {
            return retour = "Tirer la langue";
        }
        if (status == 16) {
            return retour = "Faire un signe de la main";
        }                
        if (status == 29) {
            return retour = "Trembler";
        }
        if (status == 36) {
            return retour = "Donner un coup de pied";
        }
        if (status == 35) {
            return retour = "Frapper";
        }
        if (status == 38) {
            return retour = "Parer un coup";
        }
        if (status == 18) {
            return retour = "Faire un geste de la tete";
        }
        if (status == 19) {
            return retour = "Faire un signe de la croix";
        }
        if (status == 20) {
            return retour = "Faire un geste de salut";
        }
        if (status == 27) {
            return retour = "Rire";
        }
       else {
            return retour = "Crier";
        }
    }
}
