/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilitaires;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import model.ActeurNonDispo;
import model.PlateauNonDispo;
import model.Scene;
import model.SceneActeurView;

/**
 *
 * @author RickyBic
 */
public class Api {

    public static Object[] planning(List<Scene> lsc, List<PlateauNonDispo> lpnd, List<SceneActeurView> lsca, List<ActeurNonDispo> land, String datedebut, String datefin) throws Exception {
        Object[] results = new Object[4];
        List<Date> ld = new ArrayList<>();
        Date db = new SimpleDateFormat("dd/MM/yyyy").parse(datedebut);
        Date df = new SimpleDateFormat("dd/MM/yyyy").parse(datefin);
        /*-----------------------------------------------------------*/
        while (true) {
            ld.add(db);
            Calendar c = Calendar.getInstance();
            c.setTime(db);
            c.add(Calendar.DATE, 1);
            db = c.getTime();
            if (db.compareTo(df) == 0) {
                ld.add(db);
                break;
            }
        }
        /* liste date tournage sélectionées (intervalle)-------------*/
        List<Date> ldts = new ArrayList<>(); // (to use)
        ld.forEach((d) -> {
            ldts.add(d);
        });
        results[0] = ldts;
        /* liste date tournage possibles-----------------------------*/
        List<Date> ldt = new ArrayList<>(); // (to use)
        ld.stream().filter((d) -> (!isWeekend(d))).forEachOrdered((d) -> {
            ldt.add(d);
        });
        ld.clear();
        /*-----------------------------------------------------------*/
        List<HashMap<Date, Scene>> planning; // (to use)
        List<HashMap<Date, Double>> lthdt; // liste total heure scènes par date tournage (to use)
        List<Scene> lscnd = new ArrayList<>(); // liste scènes non disponibles
        double htmax = 480; // heure tournage maximale => 8h (480min) / jour
        planning = new ArrayList<>();
        lthdt = new ArrayList<>();
        for (Scene sc : lsc) {
            for (Date dt : ldt) {
                if (isPlateauDispo(sc, dt, lpnd) && isActeurDispo(sc, dt, lsca, land) && isSceneDispo(sc, lscnd)) {
                    if (addTotalHeureDate(dt, sc.getDuree(), htmax, lthdt)) {
                        HashMap<Date, Scene> p = new HashMap<>();
                        p.put(dt, sc);
                        planning.add(p);
                        lscnd.add(sc);
                        break;
                    }
                }
            }
        }
        results[1] = planning;
        results[2] = lthdt;
        /* liste scènes non planifiées--------------------------------*/
        List<Scene> lscnp = new ArrayList<>();
        lsc.forEach((sc) -> {
            boolean isSceneDispo = true;
            for (Scene scnd : lscnd) {
                if (Objects.equals(sc.getIdScene(), scnd.getIdScene())) {
                    isSceneDispo = false;
                }
            }
            if (isSceneDispo) {
                lscnp.add(sc);
            }
        });
        results[3] = lscnp;
        return results;
    }

    private static boolean addTotalHeureDate(Date dt, double duree, double htmax, List<HashMap<Date, Double>> lthdt) {
        double th = 0; // total heure scènes
        for (HashMap<Date, Double> thdt : lthdt) {
            if (thdt.get(dt) != null) {
                th += thdt.get(dt) + duree;
                if (th <= htmax) {
                    thdt.put(dt, th);
                    return true;
                }
                return false;
            }
        }
        HashMap<Date, Double> thdt = new HashMap<>();
        if (duree <= htmax) {
            thdt.put(dt, duree);
            lthdt.add(thdt);
            return true;
        }
        return false;
    }

    private static boolean isSceneDispo(Scene sc, List<Scene> lscnd) {
        return lscnd.stream().noneMatch((s) -> (Objects.equals(s.getIdScene(), sc.getIdScene())));
    }

    private static boolean isWeekend(Date date) {
        Calendar calendar = Calendar.getInstance(); // create a calendar object
        calendar.setTime(date); // set the calendar to the date object
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;
    }

    private static boolean isPlateauDispo(Scene sc, Date dt, List<PlateauNonDispo> lpnd) {
        return lpnd.stream().filter((pnd) -> (Objects.equals(pnd.getIdPlateau(), sc.getIdPlateau()))).map((pnd) -> new Date(pnd.getDateNonDispo().getTime())).noneMatch((dnd) -> (dt.compareTo(dnd) == 0));
    }

    private static boolean isActeurDispo(Scene sc, Date dt, List<SceneActeurView> lsca, List<ActeurNonDispo> land) {
        return lsca.stream().filter((sca) -> (Objects.equals(sca.getIdScene(), sc.getIdScene()))).noneMatch((sca) -> (!land.stream().filter((and) -> (Objects.equals(and.getIdActeur(), sca.getIdActeur()))).map((and) -> new Date(and.getDateNonDispo().getTime())).noneMatch((dnd) -> (dt.compareTo(dnd) == 0))));
    }

}
