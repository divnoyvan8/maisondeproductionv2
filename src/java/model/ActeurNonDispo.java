/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author RickyBic
 */
@Entity
public class ActeurNonDispo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /**
     * Unnecessary
     */
    private Integer rowid;
    private Integer idActeur;
    private Date dateNonDispo;

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public Integer getIdActeur() {
        return idActeur;
    }

    public void setIdActeur(Integer idActeur) {
        this.idActeur = idActeur;
    }

    public Date getDateNonDispo() {
        return dateNonDispo;
    }

    public void setDateNonDispo(Date dateNonDispo) {
        this.dateNonDispo = dateNonDispo;
    }

}
