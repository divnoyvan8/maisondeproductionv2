/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author RickyBic
 */
@Entity
public class PlateauNonDispo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    /**
     * Unnecessary
     */
    private Integer rowid;
    private Integer idPlateau;
    private Date dateNonDispo;

    public Integer getRowid() {
        return rowid;
    }

    public void setRowid(Integer rowid) {
        this.rowid = rowid;
    }

    public Integer getIdPlateau() {
        return idPlateau;
    }

    public void setIdPlateau(Integer idPlateau) {
        this.idPlateau = idPlateau;
    }

    public Date getDateNonDispo() {
        return dateNonDispo;
    }

    public void setDateNonDispo(Date dateNonDispo) {
        this.dateNonDispo = dateNonDispo;
    }

}
