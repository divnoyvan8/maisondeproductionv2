<%-- 
    Document   : ajoutaction
    Created on : 1 mars 2023, 08:40:54
    Author     : P14A_77_Michael
--%>


<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.util.ArrayList"%>
<%int idScene = (Integer) request.getAttribute("idScene");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>


    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />





    </head>

    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">
                                            <li class="active"> <a style="font-size:19px;"  href="<%=request.getContextPath()%>/">Accueil</a> </li>
                                            <li><a style="font-size:19px;" href="">Planning</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="assets/icon/email.png" />nymalaza@gmail.com</li>
                                    <li style="font-size:17px;"><img src="assets/icon/loc.png" />Location</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>
        <br>
        <br>
        <br>

    <body>


        <a href="acceuil.jsp"></a>
        <div class="container-fluid">
            <center><h3 class="text-dark mb-2">Ajouter Action</h3></center>

            <div class="col-lg-8"></div>
            <div class="col-lg-12">
                <form action="<%=request.getContextPath()%>/ajoutAction">

                    <div class="row">
                        <div class="col">
                            <div class="card shadow mb-2">
                                <div class="card-header py-2">
                                    <p class="text-primary m-0 fw-bold" style="border-color: #1cc88a">Insertion scene</h3></p>
                                </div>
                                <div class="card-body">                                                     
                                    <div class="row">
                                        <div class="col">                                                                                     
                                            <div class="mb-3"><input value="<%out.println(idScene);%>" name="idScene" hidden=""></div>                     
                                            <label class="form-label"><strong>Action: </strong><br></label><input class="form-control" id="signature" type="text" name="scenario"required></p>                                            
                                            <label class="form-label"><strong>Duree: </strong><br></label><input class="form-control" id="signature" type="text" name="duree" required></p>
                                            <input style="margin-left: 30px"class="btn btn-primary btn-sm" type="submit" style="background: rgb(16,5,20);" value="Enregistrer"/>
                                            
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="col-lg-4"></div>

        </div>
    </body>

    <footer>
        <div id="contact" class="footer">
            <div class="container">
                <div class="row pdn-top-30">
                    <div class="col-md-12 ">
                        <div class="footer-box">
                            <div class="headinga">
                                <h3>Address</h3>
                                <span>Paris | France </span>
                                <p>(+261) 8522369417
                                    <br>jolympique@gmail.com</p>
                            </div>
                            <ul class="location_icon">
                                <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                                <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                            </ul>
                            <div class="menu-bottom">
                                <ul class="link">
                                    <li> <a href="">Actualiser</a></li>
                                    <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <div class="container">
                    <marquee> <p>© RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/jquery-3.0.0.min.js"></script>
    <script src="js/plugin.js"></script>

    <script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/custom.js"></script>

    <script src="js/owl.carousel.js"></script>
</body>

</html>
