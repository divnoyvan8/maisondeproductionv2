<%-- 
    Document   : listeClient
    Created on : 3 f�vr. 2023, 16:46:49
    Author     : P14A_77_Michael
--%>

<%@page import="model.Acteur"%>
<%@page import="model.Plateau"%>
<%@page import="model.DateTournage"%>
<%@page import="java.util.List"%>
<%@page import="model.Projet"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%
    Projet p = (Projet) request.getAttribute("p");
    List<DateTournage> ldt = (List<DateTournage>) request.getAttribute("ldt");
    List<Plateau> lpl = (List<Plateau>) request.getAttribute("lpl");
    List<Acteur> la = (List<Acteur>) request.getAttribute("la");
%>

<html>

    <head>
        <meta charset="utf-8">
        <title>Article</title>
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/boostrap1/css/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/css1/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/style.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/csss/responsive.css">
        <link rel="stylesheet" type="text/css"href="http://localhost:8080/Actu_-_Copie/assets/csss/Search.css">
        <link rel="stylesheet" type="text/css" href="http://localhost:8080/Actu_-_Copie/assets/css/bootstrap.css" />
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Basic.css'); ?>">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Footer-Clean.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="http://localhost:8080/Actu_-_Copie/assets/contact/fonts/ionicons.min.css">

    </head>
    <body class="main-layout ">
        <header>
            <div class="header">

                <div class="container">
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col logo_section">
                            <div class="full">
                                <div class="center-desk">
                                    <div class="logo">
                                        <img src="http://localhost:8080/Actu_-_Copie/assets/images/logo1.png" alt="icon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                            <div class="menu-area">
                                <div class="limit-box">
                                    <nav class="main-menu">
                                        <ul class="menu-area-main">

                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="acceuil.html"></a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/">Acceuil</a> </li>
                                            <li> <a style="font-size:19px;" href="<%=request.getContextPath()%>/planning">Planning</a> </li>
                                            <li class="search">
                                                <form action="<%=request.getContextPath()%>/recherche">
                                                    <input style="background-color: black;border-radius: 20px;color:white; border: 0.5px" type="text" name="recherche">
                                                    <button style="background: black;border: 0px" type="submit" ><img type="submit" src="http://localhost:8080/Actu_-_Copie/assets/images/search_icon.png" alt="icon" /></button>
                                                </form>
                                            </li>

                                        </ul>

                                    </nav>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 offset-md-6">
                            <div class="location_icon_bottum">
                                <ul>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/call.png" />(+261)9876543109</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/email.png" />mymovies@gmail.com</li>
                                    <li style="font-size:17px;"><img src="http://localhost:8080/Actu_-_Copie/assets/icon/loc.png" />mymovies</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </header>


    <body>

        <section class="bg-light" id="portfolio">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h4 style="font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif;"><%=p.getNom()%></h4><br>
                        <p class="text-muted">mymovies</p>
                        <li></li>
                    </div>
                </div>

                <div class="row">
                    <% for (DateTournage dt : ldt) {%>
                    <div class="col-sm-4 col-md-4 portfolio-item">
                        <div class="portfolio-hover-content">
                            <div class="portfolio-caption">
                                <h3 style="color:purple">Date :</a></h3>
                                <h6><%=dt.getDateDebut()%> - <%=dt.getDateFin()%></h6>
                                <a href="${pageContext.request.contextPath}/plannification?idProjet=<%=p.getIdProjet()%>&dateid=<%=dt.getRowid()%>" ><button class="btn btn-primary btn-sm" style="background-color:black;float: center;border-color: black">details</button></a>

                            </div>
                        </div>

                    </div>
                    <% }%>

                </div>
                <div>
                    <h3><a href="${pageContext.request.contextPath}/redirectajoutdatetournage?idProjet=<%=p.getIdProjet()%>" >Planifier</a></h3>
                    <div class="text">Planifier des sc�nes dans un "<strong>Intervalle</strong>" de temps</div>
                </div>
            <div>
            <h3><a href="${pageContext.request.contextPath}/redirectajoutcontrainte?idProjet=<%=p.getIdProjet()%>" >Ajouter contrainte</a></h3>
            <div class="text">Ajouter l'indisponibilit� d'un plateau/acteur</div>
        </div>
    </div>


    <div class="row">


        <!-- Service Block -->






    </div>

</div>
</section>

<section class="page-title" style="background-image:url(${pageContext.request.contextPath}/resources/images/background/scene.jpg)">

</section>
<!--End Page Title-->

<!-- Portfolio Page Section -->
<section class="portfolio-page-section">
    <div class="auto-container">

        <div>
            <h3><strong>Date de tournage</strong> (Intervalle)</h3>
            <br/>


            <div id="myModal" class="modal2">

                <!-- Modal content 
                <div class="modal-content" style="position: absolute;">
                    <span class="close">&times;</span>
                    <div class="billing-details">
                        <div class="shop-form">
                            <form method="get" action="${pageContext.request.contextPath}/ajout_dateTournage">
                                <div class="row clearfix">
                                    <div class="form-group" style="text-align: center;">
                                        <h3 style="margin-left: 10px;">Date</h3>
                                        <br/>
                                        <p style="white-space: pre;">     Date Debut :  <input type="date" name="dateDebut" required>
                                            <br/>Date Fin :  <input type="date" name="dateFin" required>
                                        </p>
                                        <input type="hidden" name="idProjet" value="<%=p.getIdProjet()%>">
                                        <button type="submit" class="theme-btn btn-style-one" style="float: right;"><span class="txt">Ajouter</span></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                -->

            </div>

            <div id="myModal2" class="modal3">

                <!-- Modal content -->


            </div>


        </div>


    </div>
</section>

</div>
</div>
</div>
</body>

<footer>
    <div id="contact" class="footer">
        <div class="container">
            <div class="row pdn-top-30">
                <div class="col-md-12 ">
                    <div class="footer-box">
                        <div class="headinga">
                            <h3>Address</h3>
                            <span>Paris | France </span>
                            <p>(+261) 8522369417
                                <br>jolympique@gmail.com</p>
                        </div>
                        <ul class="location_icon">
                            <li> <a href="#"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="#"><i class="fa fa-instagram"></i></a></li>

                        </ul>
                        <div class="menu-bottom">
                            <ul class="link">
                                <li> <a href="">Actualiser</a></li>
                                <li> <a href="<%=request.getContextPath()%>/login"> Deconnectez</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <marquee> <p>� RAKOTONANAHARY Maheritiana<a href="https://html.Michael/"> Michael</a> </p> </marquee>
            </div>
        </div>
    </div>
</footer>
<script src="js/jquery.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/jquery-3.0.0.min.js"></script>
<script src="js/plugin.js"></script>

<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/owl.carousel.js"></script>
</body>

</html>
